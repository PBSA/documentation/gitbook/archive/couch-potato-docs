---
description: >-
  The following are the table and view definitions for the Couch Potato
  database.
---

# Objects 1.0

{% content-ref url="tables.md" %}
[tables.md](tables.md)
{% endcontent-ref %}

{% content-ref url="views.md" %}
[views.md](views.md)
{% endcontent-ref %}

