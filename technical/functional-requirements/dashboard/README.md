# Dashboard

The dashboard is the main screen and is opened from the [Home Page](../home-page.md) as soon as the user is logged in.

![](<../../../.gitbook/assets/Screen Shot 2020-03-02 at 2.50.48 PM.png>)

The components of the dashboard are:

{% content-ref url="header.md" %}
[header.md](header.md)
{% endcontent-ref %}

{% content-ref url="sport-tabs.md" %}
[sport-tabs.md](sport-tabs.md)
{% endcontent-ref %}

{% content-ref url="league-tabs.md" %}
[league-tabs.md](league-tabs.md)
{% endcontent-ref %}

{% content-ref url="calendar.md" %}
[calendar.md](calendar.md)
{% endcontent-ref %}

{% content-ref url="notifications.md" %}
[notifications.md](notifications.md)
{% endcontent-ref %}

{% content-ref url="replay.md" %}
[replay.md](replay.md)
{% endcontent-ref %}

{% content-ref url="account-menu.md" %}
[account-menu.md](account-menu.md)
{% endcontent-ref %}

##
