# Flow Diagrams

## High Level Flow Diagrams

### Login and Navigation

![](<../../.gitbook/assets/CP Dashboard.png>)

### Game Selector

![](<../../.gitbook/assets/CP Selector.png>)

### Data Replay

![](<../../.gitbook/assets/CP Replay.png>)

##
