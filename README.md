---
description: 1.0 is the original version, and 2.0 is the current version.
---

# Couch Potato

Intially the CP was developed as PHP, Angualr JS application. Now it is replaced with a Django App with OpenAPI 2.0 REST APIs shared.

{% content-ref url="technical/functional-requirements/" %}
[functional-requirements](technical/functional-requirements/)
{% endcontent-ref %}

{% content-ref url="broken-reference" %}
[Broken link](broken-reference)
{% endcontent-ref %}

{% content-ref url="couch-potato/installation.md" %}
[installation.md](couch-potato/installation.md)
{% endcontent-ref %}

{% content-ref url="broken-reference" %}
[Broken link](broken-reference)
{% endcontent-ref %}

{% content-ref url="help/faq.md" %}
[faq.md](help/faq.md)
{% endcontent-ref %}

{% content-ref url="help/how-to-guide/" %}
[how-to-guide](help/how-to-guide/)
{% endcontent-ref %}

{% content-ref url="broken-reference" %}
[Broken link](broken-reference)
{% endcontent-ref %}

{% content-ref url="broken-reference" %}
[Broken link](broken-reference)
{% endcontent-ref %}

